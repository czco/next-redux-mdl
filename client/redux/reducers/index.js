import { combineReducers } from 'redux';
import agreement from '../modules/agreement';

export default combineReducers({
  agreement,
});
