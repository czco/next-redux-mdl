/* eslint-disable */
import { isBase64 } from "../../utils";
import fetch from "isomorphic-unfetch";

export const MODULE_NAME = "PDF";
// Action Constants
const AGREEMENT_REQUEST = `${MODULE_NAME}/AGREEMENT_REQUEST`;
const AGREEMENT_SUCCESS = `${MODULE_NAME}/AGREEMENT_SUCCESS`;
const AGREEMENT_ERROR = `${MODULE_NAME}/AGREEMENT_ERROR`;
const URL = "/api/pdf";

const RETRIES = 5;

// Action Creators
export const getRequest = () => ({ type: AGREEMENT_REQUEST });
export const getSuccess = data => ({ type: AGREEMENT_SUCCESS, data });
export const getError = error => ({ type: AGREEMENT_ERROR, error });

//in which file would this go
const fetchRetry = (url, options, n) => {
  fetch(url, options).catch(error => {
    if (n === 1) throw error;
    return fetchRetry(url, options, n - 1);

    // return setTimeout(() => {
    //   return fetchRetry(url, options, n - 1);
    // },3000)
  });
};

//can we use async await?
const fetchRetryAsync = async (url, options, n) => {
  for (let i = 0; i < n; i++) {
    try {
      return await fetch(url, options);
    } catch (err) {
      const isLastAttempt = i + 1 === n;
      if (isLastAttempt) throw err;
    }
  }
};

const getAgreement = svid => {
  //export function getAgreement(svid) {
  return async dispatch => {
    dispatch(getRequest());
    try {
      const res = await fetchRetryAsync(URL, { headers: { svid } }, RETRIES);

      if (res && res.ok) {
        let out = await res.text();

        //abstract this
        //no magic strings
        if (out && isBase64(out)) {
          out = "data:application/pdf;base64," + out;
        }
        else {
          console.log(out,isBase64(out))
          alert('not a base 64')
        }

        dispatch(getSuccess(out));
      } else {
        const error = new Error(res.statusText);
        error.code = res.status;
        throw error;
      }
    } catch (err) {
      dispatch(getError(err));
    }
  };
};

export { getAgreement };

const initial = {
  loading: false,
  loaded: false,
  error: null,
  retried: 0,
  data: null
};

// Reducer
export default (state = initial, action) => {
  switch (action.type) {
    case AGREEMENT_REQUEST: {
      // postData('http://example.com/answer', {answer: 42})
      // .then(data => console.log(JSON.stringify(data))) // JSON-string from `response.json()` call
      // .catch(error => console.error(error));
      return { ...state, loading: true, loaded: false };
    }
    case AGREEMENT_SUCCESS: {
      return { ...state, loading: false, loaded: true, data: action.data };
    }
    case AGREEMENT_ERROR: {
      return { ...state, loading: false, error: action.error };
    }

    default:
      return state;
  }
};
