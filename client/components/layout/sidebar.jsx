import React from "react";
import Link from 'next/link';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const buildUrl = key => key.replace(/\s+/g, "-").toLowerCase()
const links = [{label:"React PDF",url:'/'}];

const Sidebar = () => {
  return (
      <List component="nav">
      {links.map((item,i) => (
        <Link key={i+'nav'} href={item.url}>
          <ListItem button>
            <ListItemText primary={item.label}/>
          </ListItem>
        </Link>
      ))}
      </List>
  );
};

export default Sidebar;
