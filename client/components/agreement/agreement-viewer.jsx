/* eslint-disable */
import React, { useState, memo } from "react";
import PropTypes from "prop-types";
import { Document, Page } from "react-pdf";

const renderPages = (count, scale, width) => {
  if (!count) return null;
  let pagesToRender = [];

  for (let i = 1; i <= count; i++) {
    pagesToRender.push(<Page width={width} scale={scale} key={`${i}-page-agreement`} pageNumber={i} />);
  }
  return pagesToRender;
};

const AgreementViewer = ({
  loader,
  onLoadError,
  file,
  scale,
  width,
  height
}) => {
  const [count, setCount] = useState(0);
  return (
    <div style={{ overflow: "auto", maxHeight: height }}>
      <Document
        file={file}
        loading={loader}
        onLoadError={onLoadError}
        onLoadSuccess={({ numPages }) => setCount(numPages)}
      >
        {renderPages(count,scale, width)}
      </Document>
    </div>
  );
};

AgreementViewer.defaultProps = {
  onLoadError: () => {},
  scale:1.0,
  width:null,
  height:600,
  loader: "Loading agreement. Please wait..."
};

AgreementViewer.propTypes = {};

export default memo(AgreementViewer);
