import React, { Component } from "react";
import { connect } from "react-redux";
import Loadable from "react-loadable";
import fetch from "isomorphic-unfetch";
import Typography from "@material-ui/core/Typography";

import { getAgreement } from "../../redux/modules/agreement";
import Loading from "./spinner";
import { isCSR, queryString } from "../../utils";
import TabsNav from "../layout/tabsnav";

const fullWidth = { minHeight: "100vh", width: "100%" };

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

const LazyViewer = Loadable({
  loader: () => import("./agreement-viewer"),
  loading: Loading
});

export class AgreementContainer extends Component {
  constructor(props) {
    super(props);
    this._loadAgreement = this._loadAgreement.bind(this);
  }
  componentDidMount() {
    setTimeout(() => {
      this._loadAgreement();
      // this.loadAbout();
    }, 2000);
  }

  ieWorkAround = data => {
    if (data == "" || data == undefined) {
      alert("Click on some other tab first");
    } else {
      //For IE using atob convert base64 encoded data to byte array
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        var byteCharacters = atob(data);
        var byteNumbers = new Array(byteCharacters.length);
        for (var i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        var blob = new Blob([byteArray], {
          type: "application/pdf"
        });
        window.navigator.msSaveOrOpenBlob(blob, fileName);
      } else {
        // Directly use base 64 encoded data for rest browsers (not IE)
        var base64EncodedPDF = data;
        var dataURI = base64EncodedPDF;
        window.open(dataURI, "_blank");
      }
    }
  };
  loadAbout = pkg => {
    fetch(`https://bundlephobia.com/api/size?package=${pkg}&record=true`, {
      method: "GET",
      mode: "cors"
    });
  };
  _loadAgreement = () => {
    const { getAgreement, loaded } = this.props;
    if (!loaded) {
      getAgreement(queryString("svid"));
    }
  };
  render() {
    const { loading, loaded, data } = this.props;

    return (
      <div>
        <TabsNav
          tabs={[
            {
              label: "react-pdf",
              cmp: (
                <React.Fragment>
                  {loading && <Loading />}
                  {isCSR && loaded && data && <LazyViewer file={data} />}
                </React.Fragment>
              )
            },
            {
              label: "iframe tag",
              cmp: <iframe style={fullWidth} src={data}></iframe>
            },
            {
              label: "embed tag",
              cmp: <embed style={fullWidth} src={data}></embed>
            }
          ]}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ agreement }) => ({ ...agreement });

const mapDispatchToProps = dispatch => ({
  getAgreement: svid => dispatch(getAgreement(svid))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AgreementContainer);
