// /* eslint-disable */

const fs = require("fs");
const path = require("path");

// export default function handle(req, res) {
//   const filePath = path.join(__dirname, "../data/sample.pdf"),
//     //file = fs.createReadStream(filePath),
//     //stat = fs.statSync(filePath),
//     data = fs.readFileSync(filePath),
//     h = req.headers,
//     q = req.query;

//   console.log("headers", h["svid"]);

//   if (h["svid"] && h["svid"] === "SV_b60d2ab9-d96b-4c31-bcb1-55a26c47433c") {
//     if (q["status"]) {
//       return res.sendStatus(q["status"]);
//     } else {
//       /*
//       res.setHeader('Content-Length', stat.size);
//       res.setHeader('Content-Type', 'application/pdf');
//       res.setHeader('Content-Disposition', 'attachment; filename=agreement.pdf');
//       return file.pipe(res);
//       */
//       return res.send(data.toString("base64"));
//     }
//   }
//   return res.sendStatus(401);
// };

export default function handle(req, res) {
  let filePath,
    data,
    h = req.headers,
    q = req.query;

  if (h["svid"] && h["svid"] === "SV_b60d2ab9-d96b-4c31-bcb1-55a26c47433c") {
    if (q["status"]) {
      return res.sendStatus(q["status"]);
    } else {
      try {
        filePath = path.resolve("./data/sample.pdf");
        data = fs.readFileSync(filePath);
        // console.log("headers", h["svid"]);
        let str = "Hello World";
        str = data.toString("base64");
        return res.send(str);
      } catch (e) {
        console.log(e);
        console.log(filePath);
      }
    }
  } else {
    res.status(401).json({message:"Unauthorized"})
  }

  // if (h["svid"] && h["svid"] === "SV_b60d2ab9-d96b-4c31-bcb1-55a26c47433c") {
  //   if (q["status"]) {
  //     return res.sendStatus(q["status"]);
  //   } else {
  //     /*
  //     res.setHeader('Content-Length', stat.size);
  //     res.setHeader('Content-Type', 'application/pdf');
  //     res.setHeader('Content-Disposition', 'attachment; filename=agreement.pdf');
  //     return file.pipe(res);
  //     */
  //     return res.send(data.toString("base64"));
  //   }
  // }

  //res.end('Hello World')
}
