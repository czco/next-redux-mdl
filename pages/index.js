import React from 'react';
import AgreementContainer from '../client/components/agreement/agreement-container'
import Layout from "../client/components/layout/layout";


const Index = () => {
    return (
        <Layout>
            <AgreementContainer/>
        </Layout>
    );
};

export default Index;
